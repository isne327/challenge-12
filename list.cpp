#include <iostream>
#include "list.h"

List::~List() 
{
	for (Node *p;!isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;	
	}
}

void List::headPush(int el) 
{
	Node *newNode;
	if (tail == 0 && head == 0) //In case list is empty
	{ 
		head = new Node(el, head, NULL);
		tail = head;
	}
	else
	{
		newNode = new Node(el, head, NULL);
		head->prev = newNode;
		head = newNode;
	}
	
}

void List::tailPush(int el)
{
	Node *newNode;

	if (tail == 0 && head == 0) //In case list is empty
	{ 
		tail = new Node(el, 0, tail);
		head = tail;
	}
	else {
		newNode = new Node(el, NULL, tail); 
		tail->next = newNode;
		tail = newNode;
	}
}

int List::headPop()
{
	int hold;
	Node *newNode;
	newNode = head;
	if (newNode->next == NULL) //in case this is the last element do nothing
	{
		cout << endl << ">>>> This is the LAST element in the list <<<<<" << endl;
	}
	else
	{
		newNode = head->prev; 
		delete newNode; //delete NULL
		newNode = head;
		head = head->next;
		head->prev = NULL;
		hold = newNode->info;
		delete newNode;
		return hold;
	}
	
}

int List::tailPop()
{
	int hold;
	Node *newNode;
	newNode = tail;
	if (tail->prev == NULL) //in case this is the last element do nothing
	{
		cout << endl << ">>>> This is the LAST element in the list <<<<<" << endl;
	}
	else 
	{
		newNode = tail->next;
		delete newNode; //delete NULL
		newNode = tail;
		tail = tail->prev;
		tail->next = NULL;
		hold = tail->info;
		delete newNode;
		return hold;
	}
}

void List::deleteNode(int el)
{
	Node *newNode, *tmp;
	newNode = head;
	if (newNode->info == el)
	{
		newNode = head->prev; //delete NULL
		delete newNode;
		newNode = head;
		head = head->next;
		head->prev = NULL;
		delete newNode;
	}
	else
		while (newNode->next->info != el) {
			newNode = newNode->next;
		}
	tmp = newNode->next;
	newNode->next = tmp->next;
	delete tmp;
}

bool List::isInList(int el)
{
	Node *newNode;
	newNode = head;
	while (newNode->info != el && newNode->next != NULL) {
		newNode = newNode->next;
	}
	if (newNode->info != el && newNode->next == NULL) {
		return false;
	}
	else
		return true;
}

void List::sort() //BigO(N^2)
{
	Node *newNode;
	Node *c, *r , *min;
	newNode = head;
		for (c = newNode; c != NULL && c->next != NULL; c = c->next)
		{
			min = c;
			for (r = c->next; r != NULL; r = r->next)
			{
				if (r->info < min->info)
				min = r;			
			}
			if (min != c)
			{
				int hold;
				hold = min->info;
				min->info = c->info;
				c->info = hold;
			}
	}
	head = newNode;
}

void List::unique()
{
	Node *c, *r, *tmp;
	c = head;
	while (c != NULL && c->next != NULL)
	{
		r = c;
		while (r->next != NULL)
		{
			if (c->info == r->next->info)
			{
				tmp = r->next;
				r->next = r->next->next;
				delete tmp;
			}
			else
			{
				r = r->next;
			}
		}
		c = c->next;
	}
}

void List::display()
{
	Node *nodenum;
	int count = 1;
	nodenum = head;
	cout << endl;
	cout << "****************"<<endl;
	while (nodenum != NULL) {
		cout << "Element no." <<  count << " = " << nodenum -> info << endl;
		nodenum = nodenum -> next;
		count++;
	}
	cout << "****************" << endl <<endl;
	cout << "Please enter a number to proceed" << endl;
	cout << "Exit enter '0'" << endl << "Headpush enter '1'" << endl << "Tailpush enter '2'" << endl << "Headpop enter '3'" << endl << "Tailpop enter '4'" << endl << "Delete enter '5'" << endl << "Check enter '6'" << endl << "Sort enter '7'" << endl << "Delete Duplicate enter 8" << endl<<endl;
}


