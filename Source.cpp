#include<iostream>

using namespace std;
template <class T>
void swap_values(T& variable1, T& variable2);
template <class T>
T index_of_smallest(const T a[], int start_index, int number_used);
template <class T>
void sort(T a[], int number_used);



int main()
{
	const int size = 5;
	int a_int[size];
	char a_char[size];
	double a_doub[size];

	//Sorting integer demo
	cout << "Input 5 random intergers" << endl;
	for (int j = 0; j<size; j++)
	{
		cin >> a_int[j];
	}
	sort(a_int,size);
	cout << "After sorted:" << endl;
	for (int i = 0; i<size; i++)
	{
		cout << a_int[i] << " ";
	}
	cout << endl << endl;

	//Sorting double demo
	cout << "Input 5 random doubles" << endl;
	for (int j = 0; j<size; j++)
	{
		cin >> a_doub[j];
	}
	sort(a_doub, size);
	cout << "After sorted:" << endl;
	for (int i = 0; i<size; i++)
	{
		cout << a_doub[i] << " ";
	}
	cout << endl << endl;

	//Sorting Charactor demo
	cout << "Input 5 random charactors" << endl;
	for (int j = 0; j<size; j++)
	{
		cin >> a_char[j];
	}
	sort(a_char, size);
	cout << "After sorted:" << endl;
	for (int i = 0; i<size; i++)
	{
		cout << a_char[i] << " ";
	}
	cout << endl << endl;
}

template <class T>
void swap_values(T& variable1, T& variable2)
{
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}

template <class T>
T index_of_smallest(const T a[], int start_index, int number_used)
{
	T min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min)
		{
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}

template <class T>
void sort(T a[], int number_used)
{
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++)
	{
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}

